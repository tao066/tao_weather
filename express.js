

/*------  Require  ------*/
var express = require("./node_modules/express");
var app = express();
var url = require("./node_modules/url");


/*------  Setting  ------*/
app.set('port', (process.env.PORT || 5000));
var server = app.listen(app.get('port'), function(){
  console.log("Node.js is listening to PORT:" + server.address().port);
});


/*------  Rooting  ------*/
app.get('/', function(req, res) {
  res.send('Hello World!');
});
app.use('/', express.static('public'));
/* send endpoint */
// app.get('/send', function(req, res) {
//   if(req.method=='GET') {
//     var url_parts = url.parse(req.url,true);
//     var match = /https:\/\/android.googleapis.com\/gcm\/send\/(.*)/
//     endpoint = match.exec(url_parts.query.endpoint)[1];
//     console.log('receive endpoint : ' + endpoint)

//     res.writeHead(200, {"Content-Type": "text/plain"});
//     res.write("accepted\n");
//   }
//   res.end();
// });

