/**
 *  app  
 *  used at:  
 *    serviceWorker  
 *      reference : https://qiita.com/k_7016/items/503fbb85c9dba80d23f7  
 *    localStrage  
 *      reference : http://webliker.info/how-to-use-localstrage/  
 *    simpleWeather  
 *      source    : https://github.com/monkeecreate/jquery.simpleWeather  
 *      reference : http://webhacck.net/archives/773552.html  
 *    pushManager  
 *      reference : https://ceblog.mediba.jp/post/152537686847/firebase-cloud-messaging-%E3%81%AE-web-push-%E3%82%92%E8%A9%A6%E3%81%97%E3%81%A6%E3%81%BF%E3%81%9F
 *  
 *  eslint-env browser, es6
 */

'use strict';

/*------  Log setting  ------*/
window.console.log     = function(i){return;};
window.console.dir     = function(i){return;};
window.console.time    = function(i){return;};
window.console.timeEnd = function(i){return;};


/*------  App  ------*/
// Define
const locations = ['Sapporo, JP', 'Sendai, JP', 'Tokyo, JP', 'Osaka, JP', 'Fukuoka, JP', 'Naha, JP'];


// Query Weather Method
$(function() {
  var h = $(window).height();
  var reloadFalied = false;

  // Loading Method
  $('#wrap').css('display','none');
  $('#loader-bg ,#loader').height(h).css('display','block');

  // Load Weather Method
  reloadFalied = loadWeather();

  // Auto Reload Method
  setInterval(function() {
    reloadFalied = reloadWeather(reloadFalied);
  }, 60000);

  // Manual Reload Method
  $('.reload-btn-pricing').click(function() {
    reloadFalied = loadWeather();
  });

  // Add City Method
  // Show Form
  $('#add-city-btn-pricing').click(function() {
    var values = JSON.parse(localStorage.getItem('Key'));
    var results;
    if (values) {
      results = locations.filter( function( location ) {
        return !values.includes(location);
      });
    } else {
      results = locations;
    }
    var html = '';
    results.forEach( function (result) {
      html += '<option>' + result + '</option>';
    });
    $('#add-city-value').html(html);
    $('#add-city-modal').fadeIn();
  });

  // Close Form
  $('#close-add-city-modal').click(function() {
    $('#add-city-modal').fadeOut();
  });

  // Submit
  $('#add-city-submit').click(function() {
    var add_val = $('#add-city-value').val();
    var values = JSON.parse(localStorage.getItem('Key'));
    var add_bool = false;
    if (!values) {
      values = [add_val];
      add_bool = true;
    } else if (locations.includes(add_val) && !values.includes(add_val)) {
      values.push(add_val);
      add_bool = true;
    }
    if (add_bool) {
      localStorage.setItem('Key', JSON.stringify(values));
      $('#weather').html('');
      reloadFalied = loadWeather();
      $('#add-city-modal').fadeOut();
    }
  });

  // Remove City Method
  // Show Form
  $('#remove-btn-pricing').click(function() {
    var values = JSON.parse(localStorage.getItem('Key'));
    if (values) {
      var results = locations.filter( function( location ) {
        return values.includes(location);
      });
      var html = '';
      results.forEach( function (result) {
        html += '<option>' + result + '</option>';
      });
      $('#remove-value').html(html);
      $('#remove-modal').fadeIn();
    }
  });

  // Close Form
  $('#close-remove-modal').click(function() {
    $('#remove-modal').fadeOut();
  });

  // Submit
  $('#remove-submit').click(function() {
    var add_val = $('#remove-value').val();
    var values = JSON.parse(localStorage.getItem('Key'));
    var remove_bool = false;
    if (values) {
      var results = values.filter( function( location ) {
        if (location === add_val) {
          remove_bool = true;
        }
        return location !== add_val;
      });
    }
    if (remove_bool) {
      localStorage.setItem('Key', JSON.stringify(results));
      $('#weather').html('');
      reloadFalied = loadWeather();
      $('#remove-modal').fadeOut();
    }
  });

  // Finished Loading Method
  $('#loader-bg').delay(900).fadeOut(800);
  $('#loader').delay(600).fadeOut(300);
  $('#wrap').css('display', 'block');
});

// Functions
// Main Weather Method
function loadWeather() {
  var values = JSON.parse(localStorage.getItem('Key'));
  if (!values) {
    values = [locations[0]];
    localStorage.setItem('Key', JSON.stringify(values));
  }
  if (navigator.onLine) {
    setWeather(values);
    $("#alert-offline").remove();
    return false;
  } else {
    getWeather(values);
    offlineFade();
    console.warn('[SimpleWeather] No import weather data because offline');
    return true;
  }
}
function setWeather(values) {
  locations.forEach(function(location) {
    var setLocation = location.replace(/\s+/g, "");
    $.simpleWeather({
      location: location,
      unit: 'c',
      success: function(weather) {
        console.log('[SimpleWeather] Load successful with: ', setLocation);
        console.dir(weather);
        if (values.includes(location)) {
          showWeather(weather);
        }
        localStorage.setItem(setLocation, JSON.stringify(weather));
      },
      error: function(err) {
        console.error('[SimpleWeather] Load failed with: ', setLocation);
        console.error('[SimpleWeather] Error message: ', err);
      }
    });
  });
}
function getWeather(values) {
  values.forEach(function(location) {
    var setLocation = location.replace(/\s+/g, "");
    var weather = JSON.parse(localStorage.getItem(setLocation));
    if (weather) {
      console.log('[LocalStorage] Load successful with: ', setLocation);
      console.dir(weather);
      showWeather(weather);
    } else {
      console.error('[LocalStorage] No set weather data with: ', setLocation);
    }
  });
}
function showWeather(weather) {
  var city = weather.city.replace(/\s+/g, "");
  var city_id = '#' + city;

  if( !($(city_id).length) ){
    var html = weatherTemplate(city);
    $('#weather').append(html);
  }

  weatherImput(weather, city_id);
}
function weatherTemplate(city) {
  var html = `
    <div class="jumbotron" id="${city}">
      <h1 class="display-5 city"></h1>
      <hr class="my-4">
      <h2 class="display-5 text"></h2>
      <div class="row">
        <div class="col-sm-6 text-center text-img"></div>
        <div class="col-sm-6 pt-2 pb-2">
          <p class="high"></p>
          <p class="low"></p>
        </div>
      </div>
    </div>
  `;
  return html;
}
function weatherImput(weather, city_id) {
  var text = weatherEncode(weather.code);
  var image_text = text.replace(/\s+/g, '');

  $(city_id + ' .city').text(weather.city);
  $(city_id + ' .text').text('Today : ' + text);
  $(city_id + ' .text-img').html(
    '<img src="images/weather-set/' + image_text + '.png" alt="' + text + ' Picture" class="img-thumbnail">'
  );
  $(city_id + ' .high').text('High : ' + weather.high + '℃');
  $(city_id + ' .low').text('Low : ' + weather.low + '℃');
}
function weatherEncode(weatherCode) {
  var code = parseFloat(weatherCode);
  switch (code) {
    case 25: // cold
    case 32: // sunny
    case 33: // fair (night)
    case 34: // fair (day)
    case 36: // hot
    case 3200: // not available
      return 'Sunny';
    case 0: // tornado
    case 1: // tropical storm
    case 2: // hurricane
    case 6: // mixed rain and sleet
    case 8: // freezing drizzle
    case 9: // drizzle
    case 10: // freezing rain
    case 11: // showers
    case 12: // showers
    case 17: // hail
    case 35: // mixed rain and hail
    case 40: // scattered showers
      return 'Rain';
    case 3: // severe thunderstorms
    case 4: // thunderstorms
    case 37: // isolated thunderstorms
    case 38: // scattered thunderstorms
    case 39: // scattered thunderstorms (not a typo)
    case 45: // thundershowers
    case 47: // isolated thundershowers
      return 'Thunderstorms';
    case 5: // mixed rain and snow
    case 7: // mixed snow and sleet
    case 13: // snow flurries
    case 14: // light snow showers
    case 16: // snow
    case 18: // sleet
    case 41: // heavy snow
    case 42: // scattered snow showers
    case 43: // heavy snow
    case 46: // snow showers
      return 'Snow';
    case 15: // blowing snow
    case 19: // dust
    case 20: // foggy
    case 21: // haze
    case 22: // smoky
      return 'Fog';
    case 24: // windy
    case 23: // blustery
    case 29: // breezy
      return 'Windy';
    case 26: // cloudy
    case 27: // mostly cloudy (night)
    case 28: // mostly cloudy (day)
    case 31: // clear (night)
      return 'Cloudy';
    case 30: // partly cloudy (day)
    case 44: // partly cloudy
      return 'Partly Cloudy';
    default:
      console.error(code + ' is underfind');
      return 'Underfind';
  }
}

// Reload Method
function reloadWeather(bool) {
  var dt = new Date();
  console.log('reload function started at: ', dt);

  var minutes = dt.getMinutes();
  if (minutes == 0 || bool) {
    console.log('[simpleWeather] Weather data updated');
    return loadWeather();
  } else {
    return false;
  }
}

// Fade Method
function offlineFade() {
  var html = `
    <div class="alert alert-warning alert-dismissible fade show" role="alert" id='alert-offline'>
      <strong>Offline</strong>
      <br>You have bad connection. Plese reload.
    </div>
  `;
  if( !($('#alert-offline').length) ){
    return $('#weather').prepend(html);
  }
}
function reloadSuccessfulFade() {
  var html = `
    <div class="alert alert-success alert-dismissible fade show" role="alert" id='alert-reload-successful'>
      <strong>Reload successful!</strong>
    </div>
  `;
  if( !($('#alert-reload-successful').length) ){
    return $('#weather').prepend(html);
  }
}


/*------  Service Worker  ------*/
// Main Method
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js').then(function(reg) {
    console.log('[ServiceWorker] Registration successful');
    console.dir(reg);
    if ('PushManager' in window) {
      setSubscribe(reg);
    } else {
      console.warn('[ServiceWorker] PushManager is not supported');
    }
  }).catch(function(err) {
    console.error('[ServiceWorker] Registration failed: ', err);
  });
} else {
  console.warn('[ServiceWorker] ServiceWorker is not supported');
}

// Functions
function setSubscribe(reg) {
  reg.pushManager.subscribe({userVisibleOnly: true}).then(function(sub) {
    console.log('[ServiceWorker] Subscribe successful');
    console.dir(sub);

    var match = /https:\/\/android.googleapis.com\/gcm\/send\/(.*)/
    var endpoint = match.exec(sub.endpoint)[1];

    console.log('[ServiceWorker] endpoint:', endpoint);
    localStorage.setItem('Endpoint', endpoint);
  }).catch(function(err) {
    console.error('[ServiceWorker] Subscribe failed: ', err);
  });
}
