/*
 * Service Worker  
 * reference : https://qiita.com/k_7016/items/503fbb85c9dba80d23f7
 */

var CACHE_NAME = 'app_weather_v0.1.0';
var cacheWhitelist = ['app_weather_v0.1.0'];

var urlsToCache = [
  '/',
  '/index.html',
  '/images/weather-set/Cloudy.png',
  '/images/weather-set/Fog.png',
  '/images/weather-set/PartlyCloudy.png',
  '/images/weather-set/Rain.png',
  '/images/weather-set/Snow.png',
  '/images/weather-set/Sunny.png',
  '/images/weather-set/Thunderstorms.png',
  '/images/weather-set/Underfind.png',
  '/images/weather-set/Windy.png',
  '/fonts/fontawesome-webfont.woff2',
  '/css/bootstrap.min.css',
  '/css/bootstrap.min.css.map',
  '/css/font-awesome.min.css',
  '/css/style.css',
  '/js/jquery.min.js',
  '/js/jquery.simpleWeather.min.js',
  '/js/bootstrap.min.js',
  '/js/bootstrap.min.js.map',
  '/js/app.js'
];

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('[ServiceWorker] Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('activate', function(event) {
  clients.claim();
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (!cacheWhitelist.includes(cacheName)) {
            caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      if (response) {
        return response;
      }
      var fetchRequest = event.request.clone();
      return fetch(fetchRequest).then(function(response) {
        if(!response || response.status !== 200) {
          return response;
        }
        var responseToCache = response.clone();
        caches.open(CACHE_NAME).then(function(cache) {
          cache.put(event.request, responseToCache);
        });
        return response;
      });
    })
  );
});

self.addEventListener('push', function(event) {
  console.log('get push data');
  console.dir(event);

  // var reader = new FileReader();
  // reader.readAsDataURL('/images/weather-set/sunny.png');

  var title = '現在の天気';
  var content = {
    body: '現在の天気は晴れです'
    // icon: reader.result
  };
  event.waitUntil(
    self.registration.showNotification(title, content, false)
  );
});

self.addEventListener("notificationclick", function(event) {
  console.log('notification clicked:' + event)
  event.notification.close();
}, false);
