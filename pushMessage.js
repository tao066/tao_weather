var request = require('./node_modules/request');
var ENV = require('./env');

var authKey = ENV.authKey();
var apiUrl = 'https://fcm.googleapis.com/fcm/send';
var endpoints = ENV.endpoint();

endpoints.forEach(function (endpoint, index) {
  if ( endpoint ){
    var options = {
      url: apiUrl,
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
        'Authorization':'key=' + authKey
      },
      json: true,
      form: {
        "to" : endpoint
      }
    };
    request(options, function (err, ret, body) {
      if (err){
        console.error('[PushMessage] Push send failed: ', index);
        console.error('[PushMessage] Error: ', err)
      } else {
        console.log('[PushMessage] Push send successful: ', index);
        console.dir(body);
      }
    });
  } else {
    console.error('[PushMessage] Please set endpoint:', index);
  }
})
